/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package dataManagement;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.bean.validators.MustMatchRegexExpression;
import com.opencsv.bean.validators.PreAssignmentValidator;
import fileManagement.converters.TimeConverter;
import fileManagement.validators.NonNegativeIntegerValidator;
import fileManagement.validators.TimeValidator;

/**
 * Represents a Transit StopTime and its properties and relationships.
 *
 * @author David Yang
 * @author Andrew Thomas
 * @author Nathan Wagenknecht - Annotations
 * @version 1.0
 * @created 10-Oct-2019 9:26:53 AM
 */
public class StopTime implements GTFSDataType, Comparable {
    private static final int SECONDS_PER_HOUR = 3600;
    private static final int MINUTES_PER_HOUR = 60;
    private static final int HOURS_PER_DAY = 24;

    /**
     * Identifies trip
     */
    @CsvBindByName(column = "trip_id", required = true)
    private String tripId;

    /**
     * Time of arrival
     */
    @PreAssignmentValidator(validator = TimeValidator.class)
    @CsvCustomBindByName(column = "arrival_time", converter = TimeConverter.class)
    private GTFSTime arrivalTime;

    /**
     * Displays the next stop to the riders
     */
    @CsvBindByName(column = "stop_headsign")
    private String stopHeadSign;

    /**
     * Time of departure
     */
    @PreAssignmentValidator(validator = TimeValidator.class)
    @CsvCustomBindByName(column = "departure_time", converter = TimeConverter.class)
    private GTFSTime departureTime;

    /**
     * Identifies stop
     */
    @CsvBindByName(column = "stop_id", required = true)
    private String stopId;

    /**
     * Sequences of stops in a single trip
     */
    @PreAssignmentValidator(validator = NonNegativeIntegerValidator.class)
    @CsvBindByName(column = "stop_sequence", required = true)
    private int stopSequence;

    /**
     * Indicates pickup method
     * Indicates pickup method. Valid options are:
     * <p>
     * 0 or empty - Regularly scheduled pickup.
     * 1 - No pickup available.
     * 2 - Must phone agency to arrange pickup.
     * 3 - Must coordinate with driver to arrange pickup.
     */
    @PreAssignmentValidator(validator = MustMatchRegexExpression.class, paramString = "(^$|[0-3])")
    @CsvBindByName(column = "pickup_type")
    private int pickupType;

    /**
     * Indicates drop off method. Valid options are:
     * <p>
     * 0 or empty - Regularly scheduled drop off.
     * 1 - No drop off available.
     * 2 - Must phone agency to arrange drop off.
     * 3 - Must coordinate with driver to arrange drop off.
     */
    @PreAssignmentValidator(validator = MustMatchRegexExpression.class, paramString = "(^$|[0-3])")
    @CsvBindByName(column = "drop_off_type")
    private int dropOffType;

    /**
     * Default Constructor
     */
    public StopTime() {
        //do not remove, this is needed for the CSV import functionality
    }

    /**
     * Constructor of StopTime Class
     *
     * @param tripId        - Id of a trip
     * @param arrivalTime   - Arrival time of a trip
     * @param departureTime - Departure time of a trip
     * @param stopId        - Id of a stop
     * @param stopSequence  - Stop sequence number
     * @param stopHeadSign  - Head sign displaying the stop
     * @param pickupType    - Type of pick up method that a rider can select
     * @param dropOffType   - Type of drop off method that a rider can select
     */
    public StopTime(String tripId, String arrivalTime, String departureTime, String stopId,
                    int stopSequence, String stopHeadSign, int pickupType, int dropOffType) {
        this.tripId = tripId;
        this.arrivalTime = new GTFSTime(arrivalTime);
        this.departureTime = new GTFSTime(departureTime);
        this.stopId = stopId;
        this.stopSequence = stopSequence;
        this.stopHeadSign = stopHeadSign;
        this.pickupType = pickupType;
        this.dropOffType = dropOffType;
    }

    /**
     * Gets the duration of time that a bus is stopped at a stop in seconds.
     *
     * @return ^^^ in seconds.
     */
    public long getDurationAtStop() {
        if (departureTime == arrivalTime) {
            return 0;
        }
        long departureSeconds = departureTime.getSecond();
        long departureMinutes = departureTime.getMinute() * MINUTES_PER_HOUR;
        long departureHours = departureTime.getHour() * SECONDS_PER_HOUR;
        long departureTimeInSeconds = departureHours + departureMinutes + departureSeconds;

        long arrivalSeconds = arrivalTime.getSecond();
        long arrivalMinutes = arrivalTime.getMinute() * MINUTES_PER_HOUR;
        long arrivalHours = arrivalTime.getHour() * SECONDS_PER_HOUR;
        long arrivalTimeInSeconds = arrivalHours + arrivalMinutes + arrivalSeconds;

        //Account for arriving and departing on different days.
        if (arrivalTime.getHour() > departureTime.getHour()) {
            // Math to get the departure time to hour 24 or greater
            long newDepartureHours = departureTime.getHour()
                    + (HOURS_PER_DAY - departureTime.getHour());
            long fixedDepartureSeconds = departureTime.getSecond();
            long fixedDepartureMinutes = departureTime.getMinute() * MINUTES_PER_HOUR;
            long fixedDepartureHours = newDepartureHours * SECONDS_PER_HOUR;
            long fixedDepartureTimeInSeconds = fixedDepartureHours
                    + fixedDepartureMinutes + fixedDepartureSeconds;
            return fixedDepartureTimeInSeconds - arrivalTimeInSeconds;
        }

        return departureTimeInSeconds - arrivalTimeInSeconds;
    }

    /**
     * To String Method.
     *
     * @return string representation of the StopTime
     */
    public String toString() {
        return tripId + "," + arrivalTime + "," + departureTime + "," + stopId + "," +
                stopSequence + "," + stopHeadSign + "," + pickupType + "," + dropOffType;
    }


    //Getters and Setters
    public GTFSTime getArrivalTime() {
        return arrivalTime;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public void setArrivalTime(GTFSTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public GTFSTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(GTFSTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public int getStopSequence() {
        return stopSequence;
    }

    public void setStopSequence(int stopSequence) {
        this.stopSequence = stopSequence;
    }

    public int getPickupType() {
        return pickupType;
    }

    public void setPickupType(int pickupType) {
        this.pickupType = pickupType;
    }

    public int getDropOffType() {
        return dropOffType;
    }

    public void setDropOffType(int dropOffType) {
        this.dropOffType = dropOffType;
    }

    public String getStopHeadSign() {
        return stopHeadSign;
    }

    public void setStopHeadSign(String stopHeadSign) {
        this.stopHeadSign = stopHeadSign;
    }


    @Override
    public int compareTo(Object o) {
        return Integer.compare(this.stopSequence, ((StopTime) o).stopSequence);
    }
}