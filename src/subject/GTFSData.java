/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package subject;

import dataManagement.Route;
import dataManagement.Stop;
import dataManagement.StopTime;
import dataManagement.Trip;
import observers.Observer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * The GTFSData concrete observer and its properties and relationships.
 * Custom data structure to store GTFS Data.
 *
 * @author Nick Weinschrott, Noah Villanueva
 * @version 1.0
 * @created 10-Oct-2019 9:10:30 AM
 */
public class GTFSData implements Subject {

    private HashMap<String, Route> routes;
    private HashMap<String, Trip> trips;
    private HashMap<String, Stop> stops;
    private LinkedList<StopTime> stopTimes;
    protected ArrayList<Observer> observers;
    private static GTFSData mGTFSData = new GTFSData();

    /**
     * Returns an ArrayList of all current observers.
     *
     * @return an ArrayList of all current observers.
     */
    public ArrayList<Observer> getObservers() {
        return observers;
    }

    /**
     * Adds a new Observer to the list of Observers.
     *
     * @param o the new Observer being added
     */
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    /**
     * Deletes an Observer from the list of Observers.
     *
     * @param o the Observer being deleted
     */
    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    /**
     * Notifies all observers that the subject has been updated.
     *
     * @author Nick Weinschrott, Noah Villanueva
     */
    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.pull(this);
        }
    }

    private enum SubjectState { }

    /**
     * Constructor for GTFSData.
     */
    private GTFSData() {
        this.routes = new HashMap<String, Route>();
        this.trips = new HashMap<String, Trip>();
        this.stops = new HashMap<String, Stop>();
        this.observers = new ArrayList<>();
        this.stopTimes = new LinkedList<>();
    }

    /**
     * Adds a Stop to the stops HashMap.
     *
     * @param stop the stop being added
     */
    public void addStop(Stop stop) {
        String key = stop.getStopId();
        this.stops.put(key, stop);
        notifyObservers();
    }

    /**
     * Add a Collection of Stop objects to the stops HashMap.
     *
     * @param stops - Collection of Stop objects to be added
     */
    public void addStops(Collection<Stop> stops) {
        for (Stop stop : stops) {
            String key = stop.getStopId();
            this.stops.put(key, stop);
        }
        notifyObservers();
    }

    /**
     * Adds a Route to the route.
     *
     * @param route the Route being added
     */
    public void addRoute(Route route) {
        String key = route.getRouteId();
        this.routes.put(key, route);
        notifyObservers();
    }

    /**
     * Add a Collection of Route objects to the routes HashMap.
     *
     * @param routes - Collection of Route objects to be added
     */
    public void addRoutes(Collection<Route> routes) {
        for (Route route : routes) {
            String key = route.getRouteId();
            this.routes.put(key, route);
        }
        notifyObservers();
    }

    /**
     * Adds a Trip to the trips HashMap.
     *
     * @param trip the trip
     */
    public void addTrip(Trip trip) {
        String key = trip.getTripId();
        this.trips.put(key, trip);
        notifyObservers();
    }

    /**
     * Add a Collection of Trip objects to the trips HashMap.
     *
     * @param trips - Collection of Trip objects to be added
     */
    public void addTrips(Collection<Trip> trips) {
        for (Trip trip : trips) {
            String key = trip.getTripId();
            this.trips.put(key, trip);
        }
        notifyObservers();
    }

    /**
     * Adds a StopTime to the Trip object it's associated with.
     *
     * @param stopTime the StopTime being added
     */
    public void addStopTime(StopTime stopTime) {
        String tripId = stopTime.getTripId();
        Trip trip = this.trips.get(tripId);

        String stopId = stopTime.getStopId();
        Stop stop = this.stops.get(stopId);

        if (trip != null) {
            trip.addStopTime(stopTime);
        }

        if (stop != null) {
            stop.addStopTime(stopTime);
        }
        this.stopTimes.add(stopTime);
        notifyObservers();
    }

    /**
     * Add a Collection of StopTime objects to the stopTimes HashMap.
     *
     * @param stopTimes - Collection of StopTime objects to be added
     */
    public void addStopTimes(Collection<StopTime> stopTimes) {
        for (StopTime stopTime : stopTimes) {
            String tripId = stopTime.getTripId();
            Trip trip = this.trips.get(tripId);

            String stopId = stopTime.getStopId();
            Stop stop = this.stops.get(stopId);

            if (trip != null) {
                trip.addStopTime(stopTime);
            }

            if (stop != null) {
                stop.addStopTime(stopTime);
            }
            this.stopTimes.add(stopTime);
        }
        notifyObservers();
    }

    /**
     * Remove route from the routes HashMap
     * @param route - route to be removed
     */
    public void removeRoute(Route route) {
        routes.remove(route.getRouteId());
    }

    /**
     * Remove stopTime from the stopTimes LinkedList
     * @param stopTime - stopTime to be removed
     */
    public void removeStopTime(StopTime stopTime) {
        stopTimes.remove(stopTime);
    }

    /**
     * Remove stop from the stops HashMap
     * @param stop - stop to be removed
     */
    public void removeStop(Stop stop) {
        stops.remove(stop.getStopId());
    }

    /**
     * Remove trip from the trips HashMap
     * @param trip - trip to be removed
     */
    public void removeTrip(Trip trip) {
        trips.remove(trip.getTripId());
    }

    /**
     * Changes the stopTime of the indicated Stop.
     *
     * @param stopId the ID of the Stop in question
     */
    public void changeStopTime(int stopId) {
        notifyObservers();
    }

    /**
     * Clears all elements from the HashMaps.
     */
    public void clear() {
        if (!routes.isEmpty()) {
            routes.clear();
        }
        if (!trips.isEmpty()) {
            trips.clear();
        }
        if (!stops.isEmpty()) {
            stops.clear();
        }
        if (!stopTimes.isEmpty()) {
            stopTimes.clear();
        }

        if (!observers.isEmpty()) {
            notifyObservers();
        }
    }

    //Getters

    /**
     * Returns the current instance of GTFSData.
     *
     * @return the current instance
     */
    public static GTFSData getInstance() {
        return mGTFSData;
    }

    /**
     * Not Implemented.
     *
     * @param routeId The Route ID of the Route to search.
     */
    public void getFutureTrips(int routeId) {
    }

    /**
     * Not Implemented.
     *
     * @param stopId The Stop ID of the Stop to search.
     * @return The next trip on the Stop provided.
     */
    public Trip getNextTripFromStop(int stopId) {
        return null;
    }

    /**
     * Not Implemented
     *
     * @param stopId The Stop ID of the Stop to search.
     * @return The number of Trips on the Stop provided.
     */
    public int getNumTrips(int stopId) {
        return 0;
    }

    /**
     * Getter for a particular Route.
     * @param routeId The Route ID of the Route to get.
     * @return The Route indicated by the given Route ID.
     */
    public Route getRoute(int routeId) {
        return routes.get(Integer.toString(routeId));
    }

    /**
     * Not Implemented yet.
     *
     * @param stopId The Stop ID for the Stop in question.
     * @return All Routes passing through the given stop.
     */
    public ArrayList<Route> getRoutesThroughStop(int stopId) {
        return null;
    }

    /**
     * Not implemented yet.
     *
     * @param tripId The Trip ID for the Trip in question.
     * @return All Trips that are similar to the given Trip.
     */
    public ArrayList<Trip> getSimilarTrips(int tripId) {
        return null;
    }

    /**
     * This method return the distance of trip.
     *
     * @param tripID the ID of the Trip that's distance is being found
     * @return the distance of the Trip
     */
    public double getTripDistance(String tripID) {
        Trip trip = trips.get(tripID);
        return trip.getTotalDistance();
    }

    /**
     * Returns a List of the Routes.
     *
     * @return a list of Route objects
     */
    public LinkedList<Route> getRoutes() {
        return new LinkedList<>(routes.values());
    }

    /**
     * Return a HashMap of the routes.
     * @return - routes
     */
    public HashMap<String, Route> getRoutesHashMap() {
        return routes;
    }

    /**
     * Returns a List of Trips.
     *
     * @return a list of Trips
     */
    public LinkedList<Trip> getTrips() {
        return new LinkedList<>(trips.values());
    }

    /**
     * Return a HashMap of the Trips.
     *
     * @return - trips
     */
    public HashMap<String, Trip> getTripsHashMap() {
        return trips;
    }

    /**
     * Returns a list of Trips.
     *
     * @return a list of Trips
     */
    public LinkedList<Stop> getStops() {
        return new LinkedList<>(stops.values());
    }

    /**
     * Return a HashMap of Stops.
     * @return - trips
     */
    public HashMap<String, Stop> getStopsHashMap() {
        return stops;
    }

    /**
     * Returns a list of StopTimes.
     *
     * @return a List of the StopTimes
     */
    public LinkedList<StopTime> getStopTimes() {
        return new LinkedList<>(stopTimes);
    }

    /**
     * Returns the Stop object associated with the stop_id passed in.
     *
     * @param stopId The stop_id of the Stop being looked for.
     * @return The matching Stop object.
     */
    public Stop getStop(String stopId) {
        return stops.get(stopId);
    }

    /**
     * Getter for a Route.
     *
     * @param routeId The ID of the Route to get.
     * @return The Route corresponding to the given Route ID.
     */
    public Route getRoute(String routeId) {
        return routes.get(routeId);
    }

    /**
     * Getter for a Trip.
     *
     * @param tripId The ID of the Trip to get.
     * @return The Trip corresponding to the given Trip ID.
     */
    public Trip getTrip(String tripId) {
        return trips.get(tripId);
    }

    /**
     * Get the stop time with the tripId and stopId
     * @param tripId - tripId of StopTime to search for
     * @param sequenceNumber - sequence number of StopTime to search for
     * @return - StopTime object with tripId and stopId
     */
    public StopTime getStopTime(String tripId, int sequenceNumber) {
        for (int i = 0; i < stopTimes.size(); i++) {
            if (stopTimes.get(i).getTripId().equals(tripId) && stopTimes.get(i).getStopSequence() == sequenceNumber) {
                return stopTimes.get(i);
            }
        }

        return null;
    }
}