package controllers;

import fileManagement.FileHandler;
import fileManagement.GTFSFiles;
import fileManagement.ImportReport;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Controller class for the 'import_menu.fxml' file.
 *
 * @author Noah Villanueva
 * @version 1.0
 * @created 13-October-2019 11:10:12 AM
 */
public class ImportMenuController {
    private Stage stage;
    private GTFSFiles gtfsFiles;
    private FileHandler fileHandler;
    private File routesFile;
    private File stopTimesFile;
    private File stopsFile;
    private File tripsFile;
    private boolean importSuccessful;
    private TextArea importReportTextArea;

    @FXML
    Button selectRoutesFile;
    @FXML
    TextField routesFileTextField;
    @FXML
    Button selectStopTimesFile;
    @FXML
    TextField stopTimesFileTextField;
    @FXML
    Button selectStopsFile;
    @FXML
    TextField stopsFileTextField;
    @FXML
    Button selectTripsFile;
    @FXML
    TextField tripsFileTextField;

    /**
     * Constructor for ImportMenuController
     */
    public ImportMenuController() {
        importSuccessful = false;
    }

    /**
     * Imports GTFS Files
     * Validates the Files, Stores the Data, then Logs the result to a file.
     */
    @FXML
    public void importGtfsFiles() {
        try {
            //Gather field values and import the files.
            gtfsFiles = new GTFSFiles(routesFile, stopTimesFile, stopsFile, tripsFile);
            ImportReport importReport = fileHandler.importGTFSData(gtfsFiles);

            //Create Log of the File Import
            String logFileName = getCurrentTime();
            File logsFolder = new File("logs");
            logsFolder.mkdirs();
            File newLog = new File(logsFolder, logFileName + ".txt");
            newLog.createNewFile();
            PrintWriter out = new PrintWriter(newLog);
            out.println(importReport.getReportMessage());
            out.close();
            System.out.println(importReport.getReportMessage());
            importReportTextArea.setText(importReport.getReportMessage());

            if (!importReport.isFileGroupValid()) {
                String alertContextText =
                        "The following files you attempted to import are corrupt:\n";
                if (!importReport.isRoutesFileValid()) {
                    alertContextText += routesFile.getName() + "\n";
                }
                if (!importReport.isStopTimesFileValid()) {
                    alertContextText += stopTimesFile.getName() + "\n";
                }
                if (!importReport.isStopsFileValid()) {
                    alertContextText += stopsFile.getName() + "\n";
                }
                if (!importReport.isTripsFileValid()) {
                    alertContextText += tripsFile.getName() + "\n";
                }
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Corrupt Files");
                alert.setContentText(alertContextText);
                // Display the error message
                alert.showAndWait();
                return;
            }
            importSuccessful = true;

            stage.close();
        } catch (NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Please Select All Necessary Files");
            alert.setContentText("You have not selected all necessary GTFS files");
            // Display the error message
            alert.showAndWait();
        } catch (FileNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Missing File");
            alert.setContentText("One of the files that you selected cannot be found. " +
                    "Did it get deleted or moved after you selected it?");
            // Display the error message
            alert.showAndWait();
        } catch (IOException e) {
            System.out.println("IO Exception Encountered when creating log file.");
        }
    }

    //Getters and Setters
    public void setFileHandler(FileHandler fileHandler) {
        this.fileHandler = fileHandler;
    }

    public FileHandler getFileHandler() {
        return fileHandler;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setImportReportTextArea(TextArea importReportTextArea) {
        this.importReportTextArea = importReportTextArea;
    }

    /**
     * Handle the pressing of the selectRouteFile button
     */
    @FXML
    public void handleSelectRoutesFilePress() {
        File routesFile = showGTFSOpenDialog();
        if (routesFile != null) {
            this.routesFile = routesFile;
            routesFileTextField.setText(routesFile.getName());
        }
    }

    /**
     * Handle the pressing of the selectRouteFile button
     */
    @FXML
    public void handleSelectStopTimesFilePress() {
        File stopTimesFile = showGTFSOpenDialog();
        if (stopTimesFile != null) {
            this.stopTimesFile = stopTimesFile;
            stopTimesFileTextField.setText(stopTimesFile.getName());
        }
    }

    /**
     * Handle the pressing of the selectRouteFile button
     */
    @FXML
    public void handleSelectStopsFilePress() {
        File stopsFile = showGTFSOpenDialog();
        if (stopsFile != null) {
            this.stopsFile = stopsFile;
            stopsFileTextField.setText(stopsFile.getName());
        }
    }

    /**
     * Handle the pressing of the selectRouteFile button
     */
    @FXML
    public void handleSelectTripsFilePress() {
        File tripsFile = showGTFSOpenDialog();
        if (tripsFile != null) {
            this.tripsFile = tripsFile;
            tripsFileTextField.setText(tripsFile.getName());
        }
    }

    /**
     * Shows the File-Open dialog for selecting GTFS files.
     *
     * @return The File chosen by the user.
     */
    private File showGTFSOpenDialog() {
        // Initialize fileChooser and its ExtensionFilter
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter(
                "GTFS Text File", "*.txt");
        fileChooser.getExtensionFilters().addAll(filter);
        fileChooser.setInitialDirectory(new File("."));
        // Prompt the user to select a file
        return fileChooser.showOpenDialog(null);
    }

    /**
     * Returns a boolean of whether or not
     * the import was successfully carried-out.
     *
     * @return importSuccessful
     */
    public boolean getImportSuccessful() {
        return importSuccessful;
    }


    /**
     * Returns the current time in a String
     *
     * @return the current time formatted by yyyy-MM-dd HH-mm-ss
     */
    public static String getCurrentTime(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    /**
     * Custom import handler for the Chicago test files.
     */
    @FXML
    public void handleImportChicagoFiles() {
        routesFile = new File("tests/Complete_GTFS_Files/GTFS_Chicago/routes.txt");
        stopTimesFile = new File("tests/Complete_GTFS_Files/GTFS_Chicago/stop_times.txt");
        stopsFile = new File("tests/Complete_GTFS_Files/GTFS_Chicago/stops.txt");
        tripsFile = new File("tests/Complete_GTFS_Files/GTFS_Chicago/trips.txt");
        importGtfsFiles();
    }

    /**
     * Custom import handler for the Eau Claire test files.
     */
    @FXML
    public void handleImportEauClaireFiles() {
        routesFile = new File("tests/Complete_GTFS_Files/GTFS_EauClaire/routes.txt");
        stopTimesFile = new File("tests/Complete_GTFS_Files/GTFS_EauClaire/stop_times.txt");
        stopsFile = new File("tests/Complete_GTFS_Files/GTFS_EauClaire/stops.txt");
        tripsFile = new File("tests/Complete_GTFS_Files/GTFS_EauClaire/trips.txt");
        importGtfsFiles();
    }

    /**
     * Custom import handler for the LAX test files.
     */
    @FXML
    public void handleImportLAXFiles() {
        routesFile = new File("tests/Complete_GTFS_Files/GTFS_LAX/routes.txt");
        stopTimesFile = new File("tests/Complete_GTFS_Files/GTFS_LAX/stop_times.txt");
        stopsFile = new File("tests/Complete_GTFS_Files/GTFS_LAX/stops.txt");
        tripsFile = new File("tests/Complete_GTFS_Files/GTFS_LAX/trips.txt");
        importGtfsFiles();
    }

    /**
     * Custom import handler for the MCTS test files.
     */
    @FXML
    public void handleImportMCTSFiles() {
        routesFile = new File("tests/Complete_GTFS_Files/GTFS_MCTS/routes.txt");
        stopTimesFile = new File("tests/Complete_GTFS_Files/GTFS_MCTS/stop_times.txt");
        stopsFile = new File("tests/Complete_GTFS_Files/GTFS_MCTS/stops.txt");
        tripsFile = new File("tests/Complete_GTFS_Files/GTFS_MCTS/trips.txt");
        importGtfsFiles();
    }
}