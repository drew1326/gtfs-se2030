/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package controllers.updateMenus;

import dataManagement.Stop;
import fileManagement.validators.LatituteValidator;
import fileManagement.validators.LongitudeValidator;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller class for the 'update_stop_menu.fxml' file.
 *
 * @author Noah Villanueva
 * @created 10-Nov-2019 01∶08∶23 PM
 */
public class UpdateStopMenuController {
    // Private variables
    private Stop stop;
    private Stage stage;

    // FXML variables
    @FXML
    TextField stopId;
    @FXML
    TextField stopName;
    @FXML
    TextField stopDesc;
    @FXML
    TextField stopLat;
    @FXML
    TextField stopLong;
    @FXML
    Button saveButton;
    @FXML
    Button cancelButton;

    /**
     * Set the text for all text fields
     */
    public void initializeTextFields() {
        stopId.setText(stop.getStopId());
        stopName.setText(stop.getStopName());
        stopDesc.setText(stop.getStopDescription());
        stopLat.setText(String.valueOf(stop.getStopLat()));
        stopLong.setText(String.valueOf(stop.getStopLon()));
    }

    /**
     * Update the stop with the new data
     */
    @FXML
    public void save() {
        LatituteValidator latValidator = new LatituteValidator();
        LongitudeValidator longValidator = new LongitudeValidator();

        if (latValidator.isValid(stopLat.getText()) && longValidator.isValid(stopLong.getText())) {
            stop = new Stop(stopId.getText(), stopName.getText(), stopDesc.getText(),
                    Double.parseDouble(stopLat.getText()), Double.parseDouble(stopLong.getText()));
            close();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid Input");
            alert.setHeaderText("Invalid Latitude or Longitude Type");
            alert.setContentText("Please ensure that both the latitude and longitude are valid.");
            alert.showAndWait();
        }

    }

    /**
     * Close the window without saving
     */
    @FXML
    public void close() {
        stage.close();
    }

    /**
     * Getter for stop
     * @return stop
     */
    public Stop getStop() {
        return stop;
    }

    /**
     * Setter for stop
     * @param stop - New stop
     */
    public void setStop(Stop stop) {
        this.stop = stop;
    }

    /**
     * Setter for stage
     * @param stage - New stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
