/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers.viewers;

import dataManagement.StopTime;
import javafx.scene.control.TextArea;
import observers.Observer;
import subject.GTFSData;

import java.util.List;

/**
 * Observer for viewing StopTimes
 *
 * @author Noah Villanueva
 * @version 1.0
 * @created 10-Nov-2019 11:03:40 PM
 */
public class StopTimesViewer implements Observer {
    private final int MAX_DISPLAYED_STOP_TIMES = 1000;
    private List<StopTime> stopTimes;
    private TextArea textArea;

    /**
     * Constructor for StopTimesViewer
     * @param textArea - TextArea to print data to
     */
    public StopTimesViewer(TextArea textArea) {
        this.textArea = textArea;
    }
    /**
     * Pull stopTime data from the specified subject and update stopTimes
     * @param gtfsData - Subject to pull data from
     */
    @Override
    public void pull(GTFSData gtfsData) {
        stopTimes = gtfsData.getStopTimes();
        StringBuilder builder = new StringBuilder();
        if (stopTimes.size() >= MAX_DISPLAYED_STOP_TIMES) {
            builder.append("WARNING: Top 1000 stop times displaying out of a total of ")
                    .append(stopTimes.size())
                    .append(".");
        }
        builder.append("Stop Times:\n");

        int i = 0;
        for (StopTime stopTime : stopTimes) {
            builder.append("Trip \"")
                    .append(stopTime.getTripId())
                    .append("\" arriving at \"")
                    .append(stopTime.getArrivalTime())
                    .append("\" and departing at \"")
                    .append(stopTime.getDepartureTime())
                    .append("\" (Sequence Number: ")
                    .append(stopTime.getStopSequence())
                    .append(")\n");
            i++;
            if (i >= MAX_DISPLAYED_STOP_TIMES) {
                break;
            }
        }

        setText(builder.toString());
    }

    //Getters and Setters
    /**
     * Get a list of stopTimes
     * @return - list of stopTimes
     */
    public List<StopTime> getStoptimes() {
        return stopTimes;
    }

    /**
     * Get the stop time with the tripId and stopId
     * @param tripId - tripId of StopTime to search for
     * @param sequenceNumber - sequence number of StopTime to search for
     * @return - StopTime object with tripId and stopId
     */
    public StopTime getStopTime(String tripId, int sequenceNumber) {
        for (int i = 0; i < stopTimes.size(); i++) {
            if (stopTimes.get(i).getTripId().equals(tripId) && stopTimes.get(i).getStopSequence() == sequenceNumber) {
                return stopTimes.get(i);
            }
        }

        return null;
    }

    /**
     * Prints data to the TextArea.
     * @param text The data to display.
     */
    public void setText(String text) {
        textArea.setText(text);
    }
}
