/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package observers.viewers;

import dataManagement.Trip;
import javafx.scene.control.TextArea;
import observers.Observer;
import subject.GTFSData;

import java.util.HashMap;

/**
 * Observer for Viewing Trips.
 *
 * @author Noah Villanueva
 * @version 1.0
 * @created 25-Oct-2019 6:03:42 AM
 */
public class TripsViewer implements Observer {
    private HashMap<String, Trip> trips;
    private TextArea textArea;

    /**
     * Constructor for TripsViewer.
     *
     * @param textArea - TextArea to print data to
     */
    public TripsViewer(TextArea textArea) {
        this.textArea = textArea;
    }
    /**
     * Pull trip data from the specified subject and update trips.
     *
     * @param gtfsData - Subject to pull data from
     */
    @Override
    public void pull(GTFSData gtfsData) {
        trips = gtfsData.getTripsHashMap();
        StringBuilder builder = new StringBuilder();
        builder.append("Trip IDs:\n");

        for (Trip trip : trips.values()) {
            builder.append(trip.getTripId()).append("\n");
        }

        setText(builder.toString());
    }

    //Getters and Setters

    /**
     * Get a list of trips.
     *
     * @return - list of trips
     */
    public HashMap<String, Trip> getTrips() {
        return trips;
    }

    /**
     * Get a trip from trips.
     *
     * @param tripId - tripId of the trip to retrieve
     * @return - Trip with tripId
     */
    public Trip getTrip(String tripId) {
        return trips.get(tripId);
    }

    /**
     * Prints data to the TextArea.
     *
     * @param text The data to display.
     */
    public void setText(String text) {
        textArea.setText(text);
    }
}
