package fileManagement.converters;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import dataManagement.GTFSTime;

/**
 * Converts a time string to a GTFSTime object.
 *
 * @Author Nathan Wagenknecht
 */
public class TimeConverter extends AbstractBeanField {

    @Override
    protected Object convert(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        if (!value.equals("")) {
            try {
                return new GTFSTime(value);
            } catch (Exception e) {
                throw new CsvConstraintViolationException("conversion of " + value + " to a GTFSTime object failed: " + e.getMessage());
            }
        }
        return null;
    }

    @Override
    protected String convertToWrite(Object object) throws CsvDataTypeMismatchException {
        if (object != null) {
            GTFSTime time = (GTFSTime) object;
            return String.format("%02d:%02d:%02d", time.getHour(), time.getMinute(), time.getSecond());
        } else {
            return "";
        }
    }
}
