/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement.verifiers;

import com.opencsv.bean.BeanVerifier;
import com.opencsv.exceptions.CsvConstraintViolationException;
import dataManagement.GTFSTime;

/**
 * Verifies syntactical correctness of StopTime object after it is parsed and validated,
 * post processing of the StopTime is also be done here.
 *
 * @author Nathan Wagenkencht
 * @param <StopTime> The StopTime type.
 */
public class StopTimeVerifier<StopTime> implements BeanVerifier<StopTime> {
    /**
     * Verifies integrity of StopTime object and performs minor post-processing if needed.
     *
     * @param stopTime object to verify and process
     * @return true if the StopTime passes verification and false otherwise
     * @throws CsvConstraintViolationException (Issue exists that warrants the StopTime be rejected)
     */
    @Override
    public boolean verifyBean(StopTime stopTime) throws CsvConstraintViolationException {
        dataManagement.StopTime tempStopTime = (dataManagement.StopTime) stopTime;

        GTFSTime arrivalTime = tempStopTime.getArrivalTime();
        GTFSTime departureTime = tempStopTime.getDepartureTime();

        if (arrivalTime == null && departureTime != null) {
            throw new CsvConstraintViolationException(
                    "conditionally required departure time is not present");
        }

        if (arrivalTime != null && departureTime == null) {
            throw new CsvConstraintViolationException(
                    "conditionally required arrival time is not present");
        }
        return true;
    }
}
