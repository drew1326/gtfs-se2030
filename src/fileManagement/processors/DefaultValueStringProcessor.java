package fileManagement.processors;

import com.opencsv.bean.processor.StringProcessor;

/**
 * A class that sets the default value for an input field.
 *
 * @author Nathan Wagenknecht
 */
public class DefaultValueStringProcessor implements StringProcessor {
    private String defaultValue;

    /**
     * Logic for processing input String.
     *
     * @param value string to process
     * @return processed string
     */
    @Override
    public String processString(String value) {
        if (value == null || value.trim().isEmpty()) {
            return defaultValue;
        }
        return value;
    }

    /**
     * default value to use in the case of null or empty input strings
     *
     * @param value default value
     */
    @Override
    public void setParameterString(String value) {
        defaultValue = value;
    }
}
