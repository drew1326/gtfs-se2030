/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement.validators;

import com.opencsv.bean.BeanField;
import com.opencsv.bean.validators.StringValidator;
import com.opencsv.exceptions.CsvValidationException;

/**
 * Validates that a time is a valid format.
 *
 * @author Nathan Wagenknecht
 */
public class TimeValidator implements StringValidator {
    private String errorMessage;
    private static final int MAX_HOUR = 29;
    private static final int MAX_MINUTE = 59;

    /**
     * Tests if the value is Valid
     *
     * @param value the value being evaluated
     * @return whether the Value is valid
     */
    @Override
    public boolean isValid(String value) {
        if (value.isBlank() || value.isEmpty() || value.equals("")) {
            return true;
        }

        String[] timeString = value.split(":");

        if (timeString.length != 3) {
            errorMessage = "unexpected number of fields in timestamp, expected 3 found "
                    + timeString.length;
            return false;
        }
        try {
            if (Integer.parseInt(timeString[0]) > MAX_HOUR || Integer.parseInt(timeString[0]) < 0) {
                errorMessage = "hours value outside of valid range [0, 29]";
                return false;
            }
        } catch (NumberFormatException e) {
            errorMessage = "hours value is not a valid integer";
            return false;
        }

        try {
            if (Integer.parseInt(timeString[1]) > MAX_MINUTE
                    || Integer.parseInt(timeString[1]) < 0) {
                errorMessage = "minutes value outside of valid range [0, 59]";
                return false;
            }
        } catch (NumberFormatException e) {
            errorMessage = "minutes value is not a valid integer";
            return false;
        }

        try {
            if (Integer.parseInt(timeString[2]) > MAX_MINUTE
                    || Integer.parseInt(timeString[2]) < 0) {
                errorMessage = "seconds value outside of valid range [0, 59]";
                return false;
            }
        } catch (NumberFormatException e) {
            errorMessage = "seconds value is not a valid integer";
            return false;

        }
        return true;
    }

    /**
     * Validates a time value.
     *
     * @param value The time being validated.
     * @param beanField the bean field
     * @throws CsvValidationException In case of an invalid time.
     */
    @Override
    public void validate(String value, BeanField beanField) throws CsvValidationException {
        if (!this.isValid(value)) {
            throw new CsvValidationException(beanField.getField().getName() +
                    " " + value + " is not a valid time: " + errorMessage);
        }
    }

    //Setter
    @Override
    public void setParameterString(String s) {

    }
}
