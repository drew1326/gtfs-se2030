/*
Copyright 2019 Fall 2019 SE2030-011 Group E

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package fileManagement;

import com.opencsv.CSVReader;
import com.opencsv.bean.BeanVerifier;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A CSV parser for GTFS files.
 *
 * @author Nathan Wagenkencht
 * @param <GTFSDataType> GTFS Object to parse
 */
public class GTFSCSVParser<GTFSDataType> {
    private boolean isFileValid;
    private List<CsvException> csvExceptions;
    private List<String> warnings;
    private List<String> errors;

    private long timeToComplete;

    private List<String> headerLine;

    private File file;

    private Class<GTFSDataType> beanClass;
    private List<GTFSDataType> beans;

    private BeanVerifier<GTFSDataType> beanVerifier;
    private CsvToBean<GTFSDataType> csvToBean;

    private static final int EXCEPTION_THRESHOLD = 15;

    /**
     * Constructor for GTFSCSVParser Class.
     *
     * @param beanClass The class in question.
     * @param beanVerifier The appropriate verifier for said class.
     * @param headerLine The header line to parse.
     */
    public GTFSCSVParser(Class<GTFSDataType> beanClass,
                         BeanVerifier<GTFSDataType> beanVerifier, List<String> headerLine) {
        this.beanClass = beanClass;
        this.beanVerifier = beanVerifier;
        this.headerLine = headerLine;
        beans = new ArrayList<>();
        csvExceptions = new ArrayList<>();
        warnings = new ArrayList<>();
        errors = new ArrayList<>();
    }

    /**
     * Processes the given csv file and returns a list of beans created from the process.
     *
     * @param file file to process
     * @return list of parsed beans
     * @throws FileNotFoundException thrown if the GTFS file cannot be found
     */
    public List<GTFSDataType> parseCSVFile(File file) throws FileNotFoundException {

        long startTime = System.currentTimeMillis();

        this.file = file;

        //assume file is valid unless header is bad or no lines were successfully parsed
        isFileValid = true;

        csvToBean = new CsvToBeanBuilder<GTFSDataType>(new FileReader(file))
                .withVerifier(beanVerifier)
                .withThrowExceptions(false)
                .withType(beanClass)
                .build();

        try {
            beans = csvToBean.parse();
        } catch (Exception e) {
            errors.add("\u001B[31m" + e.getMessage() + ": " + e.getCause() + "\u001B[0m");
        } finally {
            //if no lines were validated the whole file is invalid
            if (beans.size() == 0) {
                isFileValid = false;
            }
        }

        csvExceptions = csvToBean.getCapturedExceptions();

        try {
            CSVReader csvReaderHeaderAware = new CSVReader(new FileReader(file));
            String[] actualHeaderLine = csvReaderHeaderAware.readNext();
            if (headerLine.size() < actualHeaderLine.length) {
                warnings.add("\u001B[33mWarning: incorrect number of header fields: found " +
                        headerLine.size() + " expected " + actualHeaderLine.length + "\u001B[0m");
            } else {
                for (int i = 0; i < actualHeaderLine.length; i++) {

                    if (!actualHeaderLine[i].equals(headerLine.get(i))) {
                        warnings.add(String.format(
                                "\u001B[33mWarning: %s does not match expected value %s\u001B[0m",
                                actualHeaderLine[i], headerLine.get(i)));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvValidationException e) {
            e.printStackTrace();
        }

        timeToComplete = System.currentTimeMillis() - startTime;

        //reads CSV file and adds parsed beans to bean list
        return beans;
    }

//    /**
//     * Get the file validation and processing report, intended only for display purposes.
//     *
//     * @return readable parser report
//     */
//    public String getParserReport() {
//        StringBuilder sb = new StringBuilder();
//        sb.append(String.format("[\u001B[34m%s validation report\u001B[0m]\n", file.getName()));
//        sb.append("│\n");
//        sb.append(String.format("└───┬───┤ lines validated │ %,d/%,d\n",
//                beans.size(), beans.size() + getNumCapturedExceptions()));
//        sb.append(String.format("    ├───┤ lines ignored   │ %,d\n",
//                getNumCapturedExceptions()));
//        sb.append(String.format("    ├───┤ Errors          │ %,d\n",
//                getNumCapturedExceptions() + errors.size()));
//        sb.append(String.format("    ├───┤ Warnings        │ %,d\n",
//                warnings.size()));
//        sb.append(String.format("    ├───┤ parse time      │ %s\n",
//                formatLongInterval(timeToComplete)));
//        sb.append(String.format("    ├───┤ avg. parse time │ %fms\n",
//                (float) timeToComplete/(getNumVerifiedLines() + getNumCapturedExceptions())));
//        sb.append("    │\n");
//
//        //add warnings
//        for (int i = 0; i < warnings.size(); i++) {
//            sb.append(String.format("    ├──> %s\n", warnings.get(i)));
//        }
//
//        //add errors
//        for (int i = 0; i < errors.size(); i++) {
//            sb.append(String.format("    ├──> %s\n", errors.get(i)));
//        }
//
//        //add caught exceptions
//        for (int i = 0; i < EXCEPTION_THRESHOLD && i < getNumCapturedExceptions(); i++) {
//            sb.append(String.format("    ├──> \u001B[31mLine %-4s Error: %s\u001B[0m\n",
//                    String.format("<%,d>", getCapturedCSVExceptions().get(i).getLineNumber()),
//                    getCapturedCSVExceptions().get(i).getMessage()));
//        }
//        if (getNumCapturedExceptions() > EXCEPTION_THRESHOLD) {
//            sb.append(String.format("    ├──> +%,d more...\n",
//                    getNumCapturedExceptions() - EXCEPTION_THRESHOLD));
//        }
//        if (getNumCapturedExceptions() > 0 || errors.size() > 0 || warnings.size() > 0) {
//            sb.append("    │\n");
//        }
//        sb.append(String.format("    └──> file %s, End of Report\n\n",
//                isFileValid ? "\u001B[32msuccessfully validated\u001B[0m"
//                        : "is \u001B[31minvalid\u001B[0m"));
//        return sb.toString();
//    }

    /**
     * Get the file validation and processing report, intended only for display purposes.
     *
     * @return readable parser report
     */
    public String getParserReport() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s validation report\n", file.getName()));
        sb.append("│\n");
        sb.append(String.format("└───┬───┤ lines validated │ %,d/%,d\n",
                beans.size(), beans.size() + getNumCapturedExceptions()));
        sb.append(String.format("           ├───┤ lines ignored   │ %,d\n",
                getNumCapturedExceptions()));
        sb.append(String.format("           ├───┤ Errors          │ %,d\n",
                getNumCapturedExceptions() + errors.size()));
        sb.append(String.format("           ├───┤ Warnings        │ %,d\n",
                warnings.size()));
        sb.append(String.format("           ├───┤ parse time      │ %s\n",
                formatLongInterval(timeToComplete)));
        sb.append(String.format("           ├───┤ avg. parse time │ %fms\n",
                (float) timeToComplete/(getNumVerifiedLines() + getNumCapturedExceptions())));
        sb.append("           │\n");

        //add warnings
        for (int i = 0; i < warnings.size(); i++) {
            sb.append(String.format("           ├──> %s\n", warnings.get(i)));
        }

        //add errors
        for (int i = 0; i < errors.size(); i++) {
            sb.append(String.format("           ├──> %s\n", errors.get(i)));
        }

        //add caught exceptions
        for (int i = 0; i < EXCEPTION_THRESHOLD && i < getNumCapturedExceptions(); i++) {
            sb.append(String.format("           ├──> Line %-4s Error: %s\n",
                    String.format("<%,d>", getCapturedCSVExceptions().get(i).getLineNumber()),
                    getCapturedCSVExceptions().get(i).getMessage()));
        }
        if (getNumCapturedExceptions() > EXCEPTION_THRESHOLD) {
            sb.append(String.format("           ├──> +%,d more...\n",
                    getNumCapturedExceptions() - EXCEPTION_THRESHOLD));
        }
        if (getNumCapturedExceptions() > 0 || errors.size() > 0 || warnings.size() > 0) {
            sb.append("           │\n");
        }
        sb.append(String.format("           └──> file %s, End of Report\n\n",
                isFileValid ? "successfully validated"
                        : "is invalid"));
        return sb.toString();
    }

    /**
     * Gets all CsvExceptions from underlying csvToBean object.
     *
     * @return list of captured CsvExceptions
     */
    public List<CsvException> getCapturedCSVExceptions() {
        return csvToBean.getCapturedExceptions();
    }

    /**
     * Returns a list of all beans created during parsing of file.
     *
     * @return list of beans of specified generic type
     */
    public List<GTFSDataType> getBeans() {
        return beans;
    }

    public int getNumErrors() {
        return errors.size() + getNumCapturedExceptions();
    }

    public int getNumWarnings() {
        return warnings.size();
    }

    /**
     * Gets the validity of the file after the validation process has been completed.
     *
     * @return true if file is valid, false if invalid, and null if unvalidated
     */
    public boolean isFileValid() {
        return isFileValid;
    }

    /**
     * Returns the number of successfully verified lines (number of beans in list).
     *
     * @return number of verified lines
     */
    public int getNumVerifiedLines() {
        return beans.size();
    }

    //Getter
    public int getNumCapturedExceptions() {
        return csvExceptions.size();
    }

    private static String formatLongInterval(final long l) {
        final long hr = TimeUnit.MILLISECONDS.toHours(l);
        final long min = TimeUnit.MILLISECONDS.toMinutes(l - TimeUnit.HOURS.toMillis(hr));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(
                l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        final long ms = TimeUnit.MILLISECONDS.toMillis(
                l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min)
                        - TimeUnit.SECONDS.toMillis(sec));
        return String.format("%dh %dm %ds %dms", hr, min, sec, ms);
    }
}
