**Document any changes that you make to the UML class diagram here**

Made SystemMessages methods static

Made getRoutesThroughStops and getTrips in the Stop class return a List of routes

Made getTrips return a List of Trips in Route class

Changed ArrayList to List for the observers attribute in the Subject class

importFiles in GUIController no longer has any parameters

Rename GUIController to MainMenuController